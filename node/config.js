module.exports = {
	'REDISHOST' : '127.0.0.1',
	'REDISPASS' : '',
	'REDISPORT' : '6379',
	'TOKEN'		: '1234567',
	'sign_key'	: '400d069a791d51ada8af3e6c2979bcd7',
	'socket_port': '19967',
	'WEBSITE': 'https://x.com',
	'ssl_key': 'x/privkey.pem',
	'ssl_crt': 'x/fullchain.pem'
}
