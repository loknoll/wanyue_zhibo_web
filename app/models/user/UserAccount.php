<?php

namespace app\models\user;

use app\models\user\User;
use wanyue\basic\BaseModel;
use wanyue\traits\ModelTrait;

/**
 * TODO 用户提现账号Model
 * Class StoreCart
 * @package app\models\store
 */
class UserAccount extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cash_account';

    use  ModelTrait;


    //列表
    public static function getList($uid,$page = 0, $limit = 0 )
    {

        $model = self::where('uid',$uid)->order('id desc');
        if ($page) $model->page($page, $limit);
        $list = $model->select()->toArray();
        foreach ($list as &$item){
            unset($item['uid']);
            unset($item['addtime']);
        }
        return $list;
    }

    //单个信息
    public static function getInfo($id )
    {
        return self::where('id',$id)->find();
    }

    //添加
    public static function setAccount($uid,$type,$account,$name='',$bank='')
    {

        $addtime=time();
        $res=self::create(compact('uid','type','bank','name','account','addtime'));

        if(!$res ) return self::setErrorInfo('操作失败');

        return $res;

    }

    //编辑
    public static function editAttent($uid,$accountid,$type,$account,$name='',$bank='')
    {

        $info=self::getInfo($accountid);
        if(!$info)  return self::setErrorInfo('信息错误');
        if($info['uid']!=$uid) return self::setErrorInfo('信息错误');

        $res=$info->save(compact('type','name','account','bank'));

        if(!$res ) return self::setErrorInfo('操作失败');

        return true;

    }

    //删除
    public static function delAttent($where)
    {
        $info=self::where($where)->delete();
        return true;

    }



}