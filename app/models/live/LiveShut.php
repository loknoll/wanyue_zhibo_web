<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/18
 */

namespace app\models\live;

use wanyue\basic\BaseModel;
use think\facade\Cache;
use wanyue\traits\ModelTrait;
use app\models\live\LiveManager;
use app\models\live\Shutlist;
use app\models\user\User;
use app\Redis;

/**
 * TODO 直播禁言Model
 * Class StoreCart
 * @package app\models\store
 */
class LiveShut extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live_shut';

    use  ModelTrait;

    protected static $redis_shut = 'shut_';
    //禁言
    public static function shut($uid, $touid, $liveuid,$shutid,$showid=1)
    {
		
		//查询权限
		$user_management=LiveManager::checkmanager($uid,$liveuid);
		$touser_management=LiveManager::checkmanager($touid,$liveuid);
		//if($uid!=$liveuid) return self::setErrorInfo('无权操作');
		if($user_management<=30||$touser_management>30) return self::setErrorInfo('无权操作');
		$shutinfo =  Shutlist::where('id',$shutid)->find()->toArray();
		if(!$shutinfo){
			return self::setErrorInfo('参数错误');
		}


        if(self::isShut($touid,$liveuid)) return self::setErrorInfo('对方已被禁言');

        $actuid=$uid;
        $uid=$touid;
        $long=$shutinfo['second'];
        $add_time=time();
        $end_time=$long==0?0:time()+$long;

        $res=self::create(compact('uid','actuid','liveuid','showid','add_time','end_time'));
        if(!$res) return self::setErrorInfo('操作失败');

        $key_shut=self::$redis_shut.$liveuid;
		$data=[
			'endtime'=>$end_time,
		];
        Redis::hSet($key_shut,$touid,json_encode($data));

        return true;
    }
    //是否禁言
    public static function isShut($touid, $liveuid)
    {
        //$res= self::be(['uid'=>$touid,'liveuid'=>$liveuid]) > 0 ? 1 : 0;
		$res=self::where(['uid'=>$touid,'liveuid'=>$liveuid])->find();

		$data=[];
		if($res){
			if($res['end_time']>0){
				$time=$res['end_time'];
				if($time>time()){
					$data=[
						'endtime'=>$time,
					];						
				}else{
					self::where(['uid'=>$touid,'liveuid'=>$liveuid])->delete();
				}			
			}else{
				$data=[
					'endtime'=>0,
				];					
			}
		}
        $key_shut=self::$redis_shut.$liveuid;
        if($data){
            Redis::hSet($key_shut,$touid,json_encode($data));
			return 1;
        }else{
            Redis::hDel($key_shut,$touid);
			return 0;
        }

    }

    //删除用户
    public static function delShut($uid,$touid, $liveuid)
    {
		//查询权限
		$user_management=LiveManager::checkmanager($uid,$liveuid);
		$touser_management=LiveManager::checkmanager($touid,$liveuid);
		//if($uid!=$liveuid) return self::setErrorInfo('无权操作');
		if($user_management<=30||$touser_management>30) return self::setErrorInfo('无权操作');

        $res=self::where(['uid'=>$touid,'liveuid'=>$liveuid])->delete();

        $key_shut=self::$redis_shut.$liveuid;
        Redis::hDel($key_shut,$touid);

        return true;
    }

    //删除本场
    public static function delLiveShut( $liveuid)
    {
        $key_shut=self::$redis_shut.$liveuid;
        Redis::del($key_shut);
        return self::where(['showid'=>1,'liveuid'=>$liveuid])->delete();
    }
    //获取列表
    public static function getlist($liveuid,$p)
    {
		$p=$p>1?$p:1;
		$num=20;
		$start=($p-1)*$num;

		$time =time();
		$list=self::where('liveuid','=',$liveuid)->where('end_time','not between',[1,$time])->limit($start,$num)->select()->toArray();
		foreach($list as $k =>$t){
			$userinfo = User::getUserInfo($t['uid']);
			$t['nickname']=$userinfo['nickname'];
			$t['avatar']=$userinfo['avatar'];
			$t['end_time']=$t['end_time']==0?'永久':date('Y-m-d H:i:s',$t['end_time']);
			$list[$k]=$t;
		}
		return $list;
    }	

}