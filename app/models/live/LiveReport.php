<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/18
 */

namespace app\models\live;

use app\models\user\User;
use wanyue\basic\BaseModel;
use think\facade\Cache;

/**
 * TODO 直播举报Model
 * Class StoreCart
 * @package app\models\store
 */
class LiveReport extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live_report';

    //举报
    public static function setReport($uid,$touid,$content)
    {
        $add_time=time();
        $res=self::create(compact('uid','touid','content','add_time'));

        if (!$res) return self::setErrorInfo('举报失败');

        return true;
    }


}