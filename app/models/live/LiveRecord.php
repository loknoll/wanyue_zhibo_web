<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/18
 */

namespace app\models\live;

use wanyue\basic\BaseModel;
use think\facade\Cache;

/**
 * TODO 直播记录Model
 * Class StoreCart
 * @package app\models\store
 */
class LiveRecord extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live_record';

    //回放列表
    public static function getList($uid, $page = 0, $limit = 0)
    {
        $model = self::where('uid',$uid)->order('id desc');
        if ($page) $model->page($page, $limit);
        $list = $model->select()->toArray();

        return $list;
    }

    //获取单个信息
    public static function getInfo($where)
    {
        $res=self::where($where)->find();
        return $res? $res->toArray():[];

    }
    //获取回放地址
    public static function getUrl($id)
    {
        return self::where('id', $id)->value('url');

    }

    //添加回放记录
    public static function setRecord($info=[])
    {
        if(!$info) return false;
        $info['endtime']=time();

        return self::create($info);

    }

    //更新回放视频
    public static function setUrl($stream,$url='')
    {
        if(!$stream || $url=='') return false;

        $stream_a=explode('_',$stream);
        $uid = isset($stream_a[0])? $stream_a[0] : '0';
        $showid = isset($stream_a[1])? $stream_a[1] : '0';
        if(!$uid || !$showid) return false;

        return self::where('uid', $uid)->where('showid',$showid)->update(['url'=>$url]);
    }


}