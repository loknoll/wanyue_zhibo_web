<?php

namespace app\admin\model\shop;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use app\models\user\User;

/**
 * Class ShopApply
 * @package app\admin\model\store
 */
class ShopApply extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'uid';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'shop_apply';

    use ModelTrait;

    public static $status = [-1=>'拒绝',0=>'申请中',1=>'通过'];


    /**
     * 获取连表MOdel
     * @param $model
     * @return object
     */
    public static function getModelObject($where = [])
    {
        $model = new self();
        if (!empty($where)) {
            if (isset($where['keyword']) && $where['keyword'] != '') {
                $model = $model->where('uid|realname|tel', 'LIKE', "%{$where['keyword']}%");
            }
        }
        return $model;
    }

    /**
     * 异步获取列表
     * @param $where
     * @return array
     */
    public static function getList($where)
    {
        $model = self::getModelObject($where);
        if(isset($where['page']) && $where['page']) $model = $model->page((int)$where['page'], (int)$where['limit']);
        $model = $model->order('addtime desc');
        $data = ($data =$model->select()) && count($data) ? $data->toArray() : [];
        foreach ($data as &$item) {
            $nickname='';
            $avatar='';
            $userinfo=User::getUserInfo($item['uid'],'uid,nickname,avatar');
            if($userinfo){
                $nickname=$userinfo['nickname'];
                $avatar=$userinfo['avatar'];
            }
            $item['nickname']=$nickname;
            $item['avatar']=$avatar;
            $item['tel']=m_s($item['tel']);
            $item['cer_no']=m_s($item['cer_no']);
            $item['addtime']=date('Y-m-d H:i:s',$item['addtime']);
            $item['status_txt']=self::$status[$item['status']];;
        }

        $count = self::getModelObject($where)->count();
        return compact('count', 'data');
    }


    public static function delApply($id)
    {
        return self::del($id);
    }

}