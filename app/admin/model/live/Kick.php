<?php

namespace app\admin\model\live;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use app\models\user\User;

/**
 * Class Kick
 * @package app\admin\model\store
 */
class Kick extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live_kick';

    use ModelTrait;


    /**
     * 获取连表MOdel
     * @param $model
     * @return object
     */
    public static function getModelObject($where = [])
    {
        $model = new self();
        if (!empty($where)) {
            if (isset($where['uid']) && $where['uid'] != '') {
                $model = $model->where('uid', $where['uid']);
            }

            if (isset($where['liveuid']) && $where['liveuid'] != '') {
                $model = $model->where('liveuid', $where['liveuid']);
            }
        }
        return $model;
    }

    /**
     * 异步获取列表
     * @param $where
     * @return array
     */
    public static function getList($where)
    {
        $model = self::getModelObject($where);
        if(isset($where['page']) && $where['page']) $model = $model->page((int)$where['page'], (int)$where['limit']);
        $model = $model->order('id desc');

        $data = ($data =$model->select()) && count($data) ? $data->toArray() : [];
        foreach ($data as &$item) {
            $uid_name='已删除';
            $userinfo=User::getUserInfo($item['uid'],'uid,nickname');
            if($userinfo){
                $uid_name=$userinfo['nickname'].' ('.$userinfo['uid'].')';
            }
            $item['uid_name']=$uid_name;

            $liveuid_name='已删除';
            $liveuidinfo=User::getUserInfo($item['liveuid'],'uid,nickname');
            if($liveuidinfo){
                $liveuid_name=$liveuidinfo['nickname'].' ('.$liveuidinfo['uid'].')';
            }
            $item['liveuid_name']=$liveuid_name;

            $actuid_name='已删除';
            $actuserinfo=User::getUserInfo($item['actuid'],'uid,nickname');
            if($actuserinfo){
                $actuid_name=$actuserinfo['nickname'].' ('.$actuserinfo['uid'].')';
            }
            $item['actuid_name']=$actuid_name;

            $item['addtime']=date('Y-m-d H:i:s',$item['add_time']);

        }

        $count = self::getModelObject($where)->count();
        return compact('count', 'data');
    }


    public static function delid($id)
    {
        return self::del($id);
    }

}