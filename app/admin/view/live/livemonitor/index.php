{extend name="public/containermonitor"}
{block name="socket"}

{/block}
{block name="content"}

<style>
    li{
        list-style:none;
    }
    #List:after{
        content:' ';
        clear:both;
        display: block;
    }
    #List li
    {
        list-style:none;
        width:230px;
        height:505px;
        border: 1px solid #C2D1D8;
        float:left;
        margin:10px;
    }
    #List li button
    {
        margin-left:30px;
    }
    #List li span
    {
        display:block;
        text-align:center
    }
    #List li .name
    {
        width:100%;
        overflow:hidden;
        white-space:nowrap;
        text-overflow:ellipsis;
    }
    .full_btn
    {
        float: left;
        height: 30px;
        padding: 0 18px;
        background: #1dccaa;
        border-radius: 4px;
        line-height: 30px;
        text-align: center;
        color: #fff;
        font-size: 14px;
        cursor: pointer;
        text-decoration: none;
        margin-left: 10px;
    }
    .full_btn:hover
    {
        background: #356f64;
        color: #fff;
        text-decoration:none;
    }
    .xgplayer video {
        width: 100% !important;
        height: 100% !important;
    }
</style>

<script src="{__STATIC_PATH}xigua/xgplayer.js?t=1574906138" type="text/javascript"></script>
<script src="{__STATIC_PATH}xigua/xgplayer-flv.js.js" type="text/javascript"></script>
<script src="{__STATIC_PATH}xigua/xgplayer-hls.js.js" type="text/javascript"></script>
<script src="{__STATIC_PATH}xigua/player.js?t=1595656253" type="text/javascript"></script>
<script src="{__ADMIN_PATH}js/layuiList.js"></script>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15"  id="app">
        <!--列表-->
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">列表</div>
                <div class="layui-card-body">
                    <div class="layui-btn-container">
                        <a class="layui-btn layui-btn-sm" href="{:Url('index')}">首页</a>
                    </div>
                    <div>
                        <ul id="List" lay-filter="List">

                        </ul>
                    </div>
                    <div id="listpage"></div>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}
{block name="script"}
<script src="{__STATIC_PATH}socket/socket.io.js"></script>
<script> var socket = new io("{$chatserver}");</script>	
<script type="text/javascript">
console.log("{$chatserver}");
 //   var socket = new io("{$chatserver}");
    function closeRoom(roomId){
        var data2 = {"token":"1234567","roomnum":roomId};
        $.ajax({
            async: false,
            url: '{:Url('stop')}',
            data:{uid:roomId},
            dataType: "json",
            success: function(res){
                if(res.code != 0 ) {
                    layer.msg(res.msg);
                }else{
                    socket.emit("superadminaction",data2);
                    alert("房间已关闭");
                    location.reload();
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrown){
                alert('关闭失败，请重试');
            }
        });
    }
    function warning(roomId,content){
        var data2 = {"token":"1234567","roomnum":roomId,'content':content};
		console.log(data2);
		socket.emit("systemadminmsg",data2);
		layer.msg("通知成功");
    }
    function warningpop(roomId){
		console.log(roomId);
		layer.prompt(function(val, index){
			warning(roomId,val);
			layer.close(index);
		});		
    }	
    function selllist(roomId){
		console.log($eb);
		$eb.createModalFrame('在售商品',layList.Url({a:'selllist',p:{uid:roomId}}));
    }		
    function getList(page,type=0){
        $.ajax({
            url:'{:Url('getlist')}',
            type:'POST',
            data:{page:page},
            dataType: 'json',
            error:function(){

            },
            success:function(data){
                var html='';

                var list=data.data;
                var count=data.count;
                var nums=list.length;
                for(var i=0;i<nums;i++){
                    var v=list[i];
                    html+='<li class="mytd">\n' +
                        '                                <!--<span>开播时长:</span>-->\n' +
                        '                                <div  id="'+v.uid+'" style="width:230px;height:400px;"></div>\n' +
                        '                                <span class="name">主播:'+v.nickname+'</span>\n' +
                        '                                <span>房间号:'+v.uid+'</span>\n' +
                        '                                <div style="text-align:center;">\n' +
                        '                                    <a  onclick="closeRoom('+v.uid+')" class="btn btn-xs btn-warning">关闭</a>\n' +
                        '                                    <a  onclick="warningpop('+v.uid+')" class="btn btn-xs btn-warning">警告</a>\n' +
                        '                                </div>\n' +
                        '                            </li>\n' +
                        '                            <script type="text/javascript">\n' +
                        '                                (function(){\n' +
                        '                                    xgPlays("'+v.uid+'","'+v.pull+'");\n' +
                        '                                })()\n' +
                        '                            <\/script>\n';;
                }
                $('#List').html(html);

                if(type==1){
                    if(count<=5){
                        return !1;
                    }
                    layui.use('laypage', function(){
                        var laypage = layui.laypage;
                        //执行一个laypage实例
                        laypage.render({
                            elem: 'listpage' //注意，这里是 ID，不用加 # 号
                            ,count: count //数据总数，从服务端得到
                            ,limit: 5 //每页显示的条数
                            ,limits: [] //每页条数的选择项
                            ,jump: function(obj, first){
                                //obj包含了当前分页的所有参数，比如：
                                //console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                //console.log(obj.limit); //得到每页显示的条数

                                //首次不执行
                                if(!first){
                                    //do something
                                    var page=obj.curr;
                                    getList(page);
                                }
                            }
                        });
                    });
                }
            }
        });
    }

    (function(){
        getList(1,1);
    })()

    $('.conrelTable').find('button').each(function () {
        var type=$(this).data('type');
        $(this).on('click',function () {
            action[type] && action[type]();
        })
    })


</script>
{/block}
