{extend name="public/container"}
{block name="head_top"}
<script src="{__ADMIN_PATH}frame/js/ajaxfileupload.js"></script>
<script src="{__ADMIN_PATH}plug/validate/jquery.validate.js"></script>
<style>
    .wrapper-content {
        padding: 0 !important;
    }
    .layui-form-item{
        margin-bottom: 10px;
    }
</style>
{/block}
{block name="content"}
<div class="row">
   <div class="col-sm-12 panel panel-default" >
       <div class="panel-body" style="padding: 30px">
           <form class="form-horizontal layui-form" id="signupForm">
               <input type="hidden"  id="uid" value="{$data.uid}">
               <div class="layui-form-item">
                   <label class="layui-form-label">姓名</label>
                   <div class="layui-input-block">
                       <div class="layui-form-mid">{$data.realname}</div>
                   </div>
               </div>
               <div class="layui-form-item">
                   <label class="layui-form-label">电话</label>
                   <div class="layui-input-block">
                       <div class="layui-form-mid">{$data.tel}</div>
                   </div>
               </div>
               <div class="layui-form-item">
                   <label class="layui-form-label">身份证号</label>
                   <div class="layui-input-block">
                       <div class="layui-form-mid">{$data.cer_no}</div>
                   </div>
               </div>

               <div class="layui-form-item">
                   <label class="layui-form-label">证件正面照</label>
                   <div class="layui-input-block">
                       {if $data.cer_f}
                       <img src="{$data.cer_f}" class="layui-icon" style="max-width:100px;max-height:100px">
                       {/if}
                   </div>
               </div>

               <div class="layui-form-item">
                   <label class="layui-form-label">证件反面照</label>
                   <div class="layui-input-block">
                       {if $data.cer_b}
                       <img src="{$data.cer_b}" class="layui-icon" style="max-width:100px;max-height:100px">
                       {/if}
                   </div>
               </div>

               <div class="layui-form-item">
                   <label class="layui-form-label">证件手持照</label>
                   <div class="layui-input-block">
                       {if $data.cer_h}
                       <img src="{$data.cer_h}" class="layui-icon" style="max-width:100px;max-height:100px">
                       {/if}
                   </div>
               </div>

               <div class="layui-form-item">
                   <label class="layui-form-label">营业执照</label>
                   <div class="layui-input-block">
                       {if $data.business}
                       <img src="{$data.business}" class="layui-icon" style="max-width:100px;max-height:100px">
                       {/if}
                   </div>
               </div>

               <div class="layui-form-item">
                   <label class="layui-form-label">许可证</label>
                   <div class="layui-input-block">
                       {if $data.license}
                       <img src="{$data.license}" class="layui-icon" style="max-width:100px;max-height:100px">
                       {/if}
                   </div>
               </div>

               <div class="layui-form-item">
                   <label class="layui-form-label">其他证件</label>
                   <div class="layui-input-block">
                       {if $data.other}
                       <img src="{$data.other}" class="layui-icon" style="max-width:100px;max-height:100px">
                       {/if}
                   </div>
               </div>

               <div class="layui-form-item">
                   <label class="layui-form-label">状态</label>
                   <div class="layui-input-block">
                       <input type="radio" name="status" value="-1" title="拒绝" {if $data.status eq -1}checked{/if}>
                       <input type="radio" name="status" value="0" title="待审核" {if $data.status eq 0}checked{/if}>
                       <input type="radio" name="status" value="1" title="通过" {if $data.status eq 1}checked{/if}>
                   </div>
               </div>

               <div class="layui-form-item layui-form-text">
                   <label class="layui-form-label">审核意见</label>
                   <div class="layui-input-block">
                       <textarea id="reason" name="reason" placeholder="请输入内容" class="layui-textarea">{$data.reason}</textarea>
                   </div>
               </div>
               <div class="form-actions">
                   <div class="row">
                       <div class="col-md-offset-4 col-md-9">
                           <button type="button" class="btn btn-w-m btn-info save_news">保存</button>
                       </div>
                   </div>
               </div>
           </form>
       </div>
   </div>
</div>
<script src="{__ADMIN_PATH}js/layuiList.js"></script>
{/block}
{block name="script"}
<script>
    layui.use('form', function(){
        var form = layui.form;
        form.render();
    });
            /**
            * 提交
            * */
            $('.save_news').on('click',function(){
                var list = {};

                list.uid = $('#uid').val();
                list.reason = $('#reason').val();
                list.status = $("input[name='status']:checked").val();

                var data = {};
                var index = layList.layer.load(1, {
                    shade: [0.5,'#fff'] //0.1透明度的白色背景
                });;
                $.ajax({
                    url:"{:Url('update')}",
                    data:list,
                    type:'post',
                    dataType:'json',
                    success:function(re){
                        layer.close(index);
                        if(re.code == 200){
                            $eb.message('success',re.msg);
                            //location.reload();
                            setTimeout(function (e) {
                                parent.$(".J_iframe:visible")[0].contentWindow.location.reload();
                            },600)
                        }else{
                            $eb.message('error',re.msg);
                        }
                    },
                    error:function () {
                        layer.close(index);
                    }
                })
            });
        </script>
{/block}