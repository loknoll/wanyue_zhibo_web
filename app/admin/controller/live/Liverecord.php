<?php

namespace app\admin\controller\live;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\live\LiveRecord as Model;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

/**
 * 直播记录
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Liverecord extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取列表
     *  @return json
     */
    public function getlist()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['uid', 0],
        ]);
        return Json::successlayui(Model::getList($where));
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $data = Model::get($id);
        if (!$data) return Json::fail('数据不存在!');


        $this->assign(compact('data'));
        return $this->fetch();
    }

    /**
     * 保存更新的资源
     *
     * @param \think\Request $request
     * @param int $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        $data = Util::postMore([
            ['uid', 0],
            ['status', 0],
            ['reason', ''],
        ], $request);

        Model::edit($data, $data['uid']);
        return Json::successful('修改成功!');
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if (!Model::delid($id))
            return Json::fail(Model::getErrorInfo('删除失败,请稍候再试!'));
        else
            return Json::successful('删除成功!');
    }
}
