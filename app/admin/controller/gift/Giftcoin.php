<?php

namespace app\admin\controller\gift;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\models\user\ChargeRules as Model;
use app\admin\model\user\User as UserModel;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

/**
 * 礼物充值规则
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Giftcoin extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取列表
     *  @return json
     */
    public function getlist()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
        ]);
        return Json::successlayui(Model::getLists($where));
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     * @return \think\Response
     */
    public function edit($id)
    {

		
		
        $c = Model::get($id);
        if (!$c) return Json::fail('数据不存在!');
        $field = [
        //    Form::radio('name', '名称', $c->getData('type'))->options([['label' => '普通', 'value' => 0],['label' => '豪华', 'value' => 1]]),
            Form::input('name', '名称', $c->getData('name')),
            Form::input('coin', '充值数量', $c->getData('coin')),
            Form::input('money', '金额', $c->getData('money')),
            Form::input('give', '赠送', $c->getData('give')),
            Form::input('list_order', '排序', $c->getData('list_order')),
        ];
        $form = Form::make_post_form('编辑礼物充值规则', $field, Url::buildUrl('update', array('id' => $id)), 2);

        $this->assign(compact('form'));
        return $this->fetch('public/form-builder');		
    }

    /**
     * 保存更新的资源
     *
     * @param \think\Request $request
     * @param int $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        $data = Util::postMore([
            ['id', 0],
            ['name', 0],
            ['coin', 0],
            ['money', 0],
            ['give', 0],
            ['list_order', 0],
        ], $request);
        $info = Model::get($data['id']);
        $data['addtime']=time();

        Model::edit($data, $data['id']);


        return Json::successful('修改成功!');
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {

        if (!Model::delid($id))
            return Json::fail(Model::getErrorInfo('删除失败,请稍候再试!'));
        else
            return Json::successful('删除成功!');
    }
	
    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $field = [
            Form::input('name', '名称'),
            Form::input('coin', '充值数量', 1),
            Form::input('money', '金额', 1.00),
            Form::input('give', '赠送', 0),
            Form::input('list_order', '排序',99),
        ];
        $form = Form::make_post_form('添加礼物充值规则', $field, Url::buildUrl('save'), 2);
        $this->assign(compact('form'));
        return $this->fetch('public/form-builder');
    }

    /**
     * 保存新建的资源
     *
     * @param \think\Request $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = Util::postMore([
            ['name', 0],
            ['coin', 0],
            ['money', 0],
            ['give', 0],
            ['list_order', 0],
        ], $request);
        if (!$data['name']) return Json::fail('请输入名称');
        if ($data['coin']<=0) return Json::fail('请输入正确充值数量');
        if ($data['money']<=0) return Json::fail('请输入正确的充值金额');
        if ($data['give']<0) return Json::fail('请输入正确的赠送数量');
        if ($data['list_order']<0) return Json::fail('请输入正确的排序值');

        $data['addtime'] = time();
        Model::create($data);
        return Json::successful('添加成功!');
    }	
}
