<?php
namespace app\api\controller\cash;

use app\models\user\User;
use app\models\cash\Votes;
use app\Request;
use wanyue\services\UtilService;

/**
 * 映票提现
 */
class VotesController
{
    /**
     * 统计
     */
    public function info(Request $request)
    {

        $uid=$request->uid();

        $userinfo=User::getUserInfo($uid,'votes,votestotal');

        $bring=$userinfo['votes'];
        $bring_total=$userinfo['votestotal'];
        $bring_ing=Votes::cashmoney($uid);
        $bring_ok=$bring_total-$bring-$bring_ing;

        return app('json')->successful(compact('bring','bring_ok','bring_ing','bring_total'));
    }

    /**
     * 列表
     */
    public function lst(Request $request)
    {
        list( $page, $limit) = UtilService::getMore([
            [['page', 'd'], 1],
            [['limit', 'd'], 20]
        ], $request, true);

        $uid=$request->uid();

        $list=Votes::getList($uid,$page,$limit);

        return app('json')->successful($list);
    }

    //提现
    public function cash(Request $request)
    {
        list($money,$accountid) = UtilService::postMore([
            ['money', 0],
            [['accountid', 'd'], 0],
        ], $request, true);

        if($money<=0 || $accountid<1) return app('json')->fail('参数错误');
        $uid=$request->uid();

        $res=Votes::cash($uid,$money,$accountid);

        if (!$res) return app('json')->fail(Votes::getErrorInfo());

        return app('json')->successful('提交成功');
    }

}