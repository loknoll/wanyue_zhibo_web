<?php

namespace app\api\controller\live;

use app\models\live\Live;
use app\models\live\Gift;
use app\models\record\Coinrecord;
use app\models\user\User;
use app\Request;
use app\Redis;
use wanyue\services\UtilService;

class GiftController
{
	
    //礼物列表
    public function giftlist(Request $request)
    {
		list($source) = UtilService::getMore([
			['source',''],
        ], $request, true);
		$uid=$request->uid();

		$where = '';
		if($source == 'wxsmall'){
			$where = 'swftype!=1';
		}
		//礼物列表
		$list = Gift::getList($where);
		$coinarr=User::getUserInfo($uid,'coin');

		$coin = $coinarr['coin'];
		return app('json')->successful(compact('coin', 'list'));
    }
    //送礼物
    public function sendgift(Request $request)
    {
        list($stream, $giftid,$liveuid,$count) = UtilService::getMore([
			['stream',''],
            [['giftid', 'd'], 0],
            [['liveuid', 'd'], 0],
            [['count', 'd'], 0],
        ], $request, true);
		$uid =$request->uid(); 
/* 		$uid=5;
		$liveuid=15;
		$count=1;
		$giftid=1;
		$stream='15_12345'; */
        if($giftid<1 || $count<1|| $liveuid<1) return app('json')->fail('参数错误');	
        $stream_a=explode('_',$stream);
        $uid_r = isset($stream_a[0])? $stream_a[0] : '0';
        $showid = isset($stream_a[1])? $stream_a[1] : '0';
		if($uid_r!=$liveuid) return app('json')->fail('参数不一致!');		
		$giftinfo = Gift::getInfo($giftid);
		if(!$giftinfo) return app('json')->fail('礼物不存在!');	
		$total = $giftinfo['coin']*$count;
		//扣费 增加费用 写记录
		$whereuser =[
            ['uid','=',$uid],
            ['coin','>=',$total],
        ];

		//扣费
		$cost = User::where($whereuser)->dec('coin',$total)->update();
		//if(!$cost)return app('json')->fail('钻石不足，请先充值!'.$uid);	
		if(!$cost)return app('json')->fail('钻石不足，请先充值!');	
		//增加映票
		$incdata=[
			'votes'=>$total,
			'votestotal'=>$total
		];
		User::incField($liveuid,$incdata);
		//写记录
		$data=[
			'type'=>1,
			'action'=>1,
			'uid'=>$uid,
			'touid'=>$liveuid,
			'giftid'=>$giftid,
			'giftcount'=>$count,
			'totalcoin'=>$total,
			'showid'=>$showid,
			'add_time'=>time()
		];		
		Coinrecord::insert($data);
		$gifttoken=md5(md5('1'.$uid.$liveuid.$giftid.$count.$total.$showid.time().rand(100,999)));
		$userinfo2=User::getUserInfo($uid,'coin');
		$userinfo3=User::getUserInfo($liveuid,'votestotal');
		
		//缓存礼物信息
		$result=array(
            "uid"=>$uid,
            "giftid"=>$giftid,
            "type"=>$giftinfo['type'],
            "giftcount"=>$count,
            "totalcoin"=>$total,
            "giftname"=>$giftinfo['name'],
            "gifticon"=>$giftinfo['icon'],
            "swftime"=>$giftinfo['swftime'],
            "swftype"=>$giftinfo['swftype'],
            "swf"=>$giftinfo['swf'],
            "coin"=>$userinfo2['coin'],
            "votestotal"=>$userinfo3['votestotal'],
            "gifttoken"=>$gifttoken,
        );			
		Redis::set($gifttoken,json_encode($result));
		//拼接返回值
		$return=[
			'coin'=>$userinfo2['coin'],
			'gifttoken'=>$gifttoken,
		];
		return app('json')->successful($return);
    }

}