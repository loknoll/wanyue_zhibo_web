<?php

namespace app\appapi\controller;

use think\facade\Route as Url;

/**
 * 基类 所有控制器继承的类
 * Class AuthController
 * @package app\admin\controller
 */
class AuthController extends AppapiBasic
{
    /**
     * 当前登陆用户信息
     * @var
     */
    protected $userInfo;

    /**
     * 当前登陆用户ID
     * @var
     */
    protected $userId;



    protected function initialize()
    {
        parent::initialize();

    }
}