<!DOCTYPE html>
<html>
<head>
    {include file="public/head"}
    <title>{$data['title']}</title>

    <link href="{__MODULE_PATH2}css/page.css?t=1577502879" rel="stylesheet">
</head>

<body class="body-white">
<div class="container tc-main">
    <div class="page_content">
        {:htmlspecialchars_decode($data['content'])}
    </div>

</div>
<!-- /container -->
{include file="public/footer"}
</body>
</html>
